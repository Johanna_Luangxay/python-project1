# -*- coding: utf-8 -*-
"""
@author: Johanna Luangxay (1939023)
@author: Le Duytam Ly (1734119)
"""
import pandas as pd
import matplotlib.pyplot as plt

class DataScience:
    """
    Deals with the data science
    """
    def __init__(self,country,connection):
        """
        Initializer

        Parameters
        ----------
        country : String
            Country to be analyse.
        connection : Obj MySQLConnection
            Connection to MySQL.

        Returns
        -------
        None.

        """
        self.__country = country.capitalize()
        self.__DbObj = connection
        self.__conn = self.__DbObj.connection_db()
        self.__DbObj.set_cursor()
        self.__DbObj.select_db()
    
    def close(self):
        """
        Closes the mysql object

        Returns
        -------
        None.

        """
        self.__DbObj.close()
        
    def compareItself(self):
        """
        Compare the country new cases, new deaths, new recovery within 6 days

        Returns
        -------
        None.

        """
        #Get mysql data into dataframe
        query = "SELECT * FROM corona_table WHERE Country_Other LIKE '{}'".format(self.__country)
        df = pd.read_sql_query(query,self.__conn)
        df = df[["date_retrieved","NewCases","NewDeaths","NewRecovered"]]
        
        print("Dataframe for 6 days key indicators of {}".format(self.__country))
        print(df,"\n")
        
        #Plot the dataframe
        df.plot(x="date_retrieved",
                y=["NewCases","NewRecovered","NewDeaths"], 
                kind="bar",
                title="6 days key indicators {}".format(self.__country))
        plt.xlabel("Date")
        plt.ylabel("3 Main Indicators")
        plt.show()
    
    def compareOneNeighbour(self):
        """
        Compare the country to its neighbour country with the longest distance border in term of new cases.

        Returns
        -------
        None.

        """
        #Get mysql data into dataframe
        distanceQuery = "SELECT MAX(distance) FROM country_borders_table WHERE country LIKE '{}'".format(self.__country)
        neighbourQuery = "SELECT neighbour FROM country_borders_table WHERE country LIKE '{}' AND distance = ({})".format(self.__country, distanceQuery)
        mainQuery = "SELECT * FROM corona_table WHERE Country_Other LIKE '{}' OR Country_Other LIKE ({})".format(self.__country,neighbourQuery)
        df = pd.read_sql_query(mainQuery,self.__conn)
        df = df[["date_retrieved","NewCases","Country_Other"]]
        
        #Change the index column values of the dataframe
        new_df = df.pivot(index="date_retrieved",columns="Country_Other",values="NewCases")
        print("Dataframe for 6 days New Cases Comparison for {} and its longuest border neighbour".format(self.__country))
        print(new_df,"\n")
                
        #Get neighbour name
        neighbour = df["Country_Other"]!=self.__country
        neighbourdf = df[neighbour]
        
        #Plot the dataframe
        new_df.plot(kind="bar")
        
        #If country is not in country border
        if neighbourdf.empty:
            print("No neighbour given")
            plt.title("6 days Newcases {}".format(self.__country))
        else:
            neighbour = neighbourdf.iat[0,2]
            plt.title("6 days Newcases Comparison {} with neighbour {}".format(self.__country, neighbour))
        
        plt.xlabel("Date")
        plt.ylabel("New Cases")
        plt.show()
    
    def compareManyNeighbours(self):
        """
        Compare data with maximum 3 other neighbour countries in term of Death/1M population.

        Returns
        -------
        None.

        """
        #Get mysql data into dataframe
        maxDateQuery = "(SELECT * FROM (SELECT DISTINCT date_retrieved FROM corona_table LIMIT 3) AS t2)"
        top2NeighbourQuery = "(SELECT neighbour FROM country_borders_table WHERE country LIKE '{}' ORDER BY distance DESC LIMIT 3)".format(self.__country)
        mainQuery = "SELECT * FROM corona_table WHERE date_retrieved IN {} AND (Country_Other LIKE '{}' OR Country_Other IN (SELECT * FROM {} AS t1))".format(maxDateQuery, self.__country,top2NeighbourQuery)
        df = pd.read_sql_query(mainQuery, self.__conn)
        df = df[["date_retrieved","Country_Other","Deaths_1Mpop"]]
        
        #Change the index column values of the dataframe
        new_df = df.pivot(index="date_retrieved",columns="Country_Other",values="Deaths_1Mpop")
        print("3 Days Deaths/1M pop Comparison {} With 3 Neighbours".format(self.__country))
        print(new_df,"\n")
        
        #Plot the dataframe
        new_df.plot(kind="bar")
        plt.title("3 days Deaths/1M pop Comparaison {} With 3 Neighbours ".format(self.__country))
        plt.xlabel("Date")
        plt.ylabel("Deaths/1M pop")
        plt.show()
        