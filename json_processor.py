# -*- coding: utf-8 -*-
"""
@author: Johanna Luangxay (1939023)
@author: Le Duy Ly (1734119)
"""

import json

class JSONProcessor:
    """
    Processes the JSON file
    """
    def __init__(self, filename):
        self.__filename = filename
        self.__jsonObj = []
    
    def read_json_file(self):
        """
        Read the json file

        Returns
        -------
        None.

        """
        try:
            json_file = open(self.__filename,"r")
            self.__jsonObj= json.load(json_file)
        except:
            print("Error")

    def get_obj_data(self):
        """
        Transforms JSON object to list

        Returns
        -------
        cleanData : List
            List of data from the JSON object.

        """
        cleanData = []
        #For every dictionnary in the JSON
        for country in self.__jsonObj:
            # For every country
            for mainCountry,dict2 in country.items():
                #For every neighbour/entry
                for neighboor,distance in dict2.items():
                    #Appends the data
                    row = (mainCountry,neighboor,distance)
                    cleanData.append(row)
        return cleanData
            