# -*- coding: utf-8 -*-
"""
@author: Johanna Luangxay (1939023)
@author: Le Duytam Ly (1734119)
"""

from web_scrapper import ScrapperAPI
from datetime import date,timedelta,datetime
from json_processor import JSONProcessor

class DBManager:
    """
    Classes that is responsable for actions related to tables and database.
    """
    
    def __init__(self, connection):  
        """
        Initializer

        Parameters
        ----------
        connection : Object MySQLConnection
            Connection for MySQL.

        Returns
        -------
        None.

        """
        self.__today = None
        self.__scrap_obj = ScrapperAPI("local_html\covid_data_2021-03-22.html","https://www.worldometers.info/coronavirus/",False)
        self.__mysql_api_obj = connection
    
    def close(self):
        """
        Closes the connection to MySQL

        Returns
        -------
        None.

        """
        self.__mysql_api_obj.close()
    
    def create_new_db(self):
        """
        Uses object mySQLAPI to create the database and the tables 

        Returns
        -------
        None.

        """
    
        #Making connection and creating/resetting the database
        self.__mysql_api_obj.connection_db()
        self.__mysql_api_obj.set_cursor()
        self.__mysql_api_obj.create_db()
        self.__mysql_api_obj.select_db()
        
        #Creating and populating country_borders_table
        self.__create_table("country_borders_table",self.__create_country_borders_schema())
        self.__populate_country_borders_table()
        #Creating empty corona table
        self.__create_table("corona_table",self.__create_corona_schema())
        
    def use_existing_db(self):
        """
        Uses MySQLAPI to select an existing database

        Returns
        -------
        None.

        """
        self.__mysql_api_obj.connection_db()
        self.__mysql_api_obj.set_cursor()
        self.__mysql_api_obj.select_db()
        
    def scrape_today(self):
        """
        Scrape data for current date

        Returns
        -------
        None.

        """
        self.__today = date.today().strftime("%Y-%m-%d")
        self.__scrap_obj = ScrapperAPI("local_html\covid_data_{}.html".format(self.__today),"https://www.worldometers.info/coronavirus/",True)
    
    def scrape_old_date(self,chosen_date):
        """
        Scrape data for specific date

        Parameters
        ----------
        chosen_date : String
            Date chosen.

        Returns
        -------
        None.

        """
        self.__today = chosen_date
        self.__scrap_obj = ScrapperAPI("local_html\covid_data_{}.html".format(self.__today),"https://www.worldometers.info/coronavirus/",False)
    
    def populate_corona_table(self):
        """
        Populate corona table with data from scrape object

        Returns
        -------
        None.

        """
        #Gets the table headers
        table_header_list = self.__scrap_obj.get_table_headers()
        #Clean headers
        table_header_insert = [e.strip("`") for e in table_header_list]
        table_header_insert =",".join(table_header_insert)
        #All 3 possible id for tables
        list_table_names = ["main_table_countries_today","main_table_countries_yesterday","main_table_countries_yesterday2"]
        #Get the specified date
        curDate = datetime.strptime(self.__today, "%Y-%m-%d")
        #Gets the date for past days
        list_dates = [self.__today, curDate - timedelta(days = 1), curDate - timedelta(days = 2)]
        
        #List containing all records for given dates
        list_three_past_days = []
        for i in range(len(list_dates)):
            table_records = self.__scrap_obj.get_table_records(list_table_names[i],list_dates[i])
            #Append the records for the 3 tables into one list
            list_three_past_days+=table_records
        
        #Insert all records in database
        self.__mysql_api_obj.populate_table("corona_table",table_header_insert,list_three_past_days,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s")
        
    def __create_table(self,table_name,schema):
        """
        Create a table

        Parameters
        ----------
        table_name : String
            Table name to be created.
        schema : String
            Name of schema to be added to.

        Returns
        -------
        None.

        """
        self.__mysql_api_obj.create_table(table_name,schema)    
    
    def __create_schema(self,table_header_list, data_type_list,*pk):
        """
        Create a schema.

        Parameters
        ----------
        table_header_list : List
            Columns name.
        data_type_list : List
            Data to be added.
        *pk : String
            Primary key.

        Returns
        -------
        schema : String
            Schema created.

        """
        schema = ""
        #Appends the column name with the data type with a comma.
        for i in range(len(table_header_list)):
            if i == len(table_header_list)-1:
                schema = schema + (table_header_list[i]+" "+data_type_list[i])
            else:
                schema = schema + (table_header_list[i]+" "+data_type_list[i]+",")
        #Give a primary key
        if pk:
            schema = schema + ",PRIMARY KEY({})".format(",".join(pk))
            
        return schema
    
    def __create_country_borders_schema(self):
        """
        Create the country_border_table schema

        Returns
        -------
        String
            Created schema.

        """
        table_header_list = ["country","neighbour","distance"]
        data_type_list = ["varchar(50)","varchar(50)","double"]
        return self.__create_schema(table_header_list, data_type_list)
    
    def __create_corona_schema(self):
        """
        Create the corona_table schema

        Returns
        -------
        String
            Created Schema.

        """
        table_header_list = self.__scrap_obj.get_table_headers()
        data_type_list = ["date","int","varchar(255)","int","int","int","int","int","int","int","int","int","int","int","int","int","varchar(255)","int","int","int"]
        return self.__create_schema(table_header_list, data_type_list,"date_retrieved","Country_Other")
       
    def __populate_country_borders_table(self):
        """
        Populate country_borders_table with data

        Returns
        -------
        None.

        """
        proc = JSONProcessor("countries_json\country_neighbour_dist_file.json")
        proc.read_json_file()
        data = proc.get_obj_data()
        self.__mysql_api_obj.populate_table("country_borders_table","country,neighbour,distance",data,"%s,%s,%s")
    
    def get_all_country_names(self):
        """
        Gets all the countries for a the corona_table

        Returns
        -------
        all_countries : list
            List containing all the country names

        """
        all_countries = self.__mysql_api_obj.select_data_from_table("corona_table","Country_Other")
        all_countries = [list(e) for e in all_countries]
        all_countries = [''.join(x).lower() for x in all_countries]
        
        return all_countries
    
