# -*- coding: utf-8 -*-
"""
@author: Johanna Luangxay (1939023)
@author: Le Duytam Ly (1734119)
"""
import mysql.connector

class MySQLAPI:
    """
    Interfacing with MySQL database
    """
    def __init__(self,host_param,use_param,password_param,db_name = None):
        """
        Initilizes the mysql_api object with a hostname, username, passward, and database name

        Parameters
        ----------
        host_param : String
            Hostname of the connection
        use_param : String
            Username of the connection
        password_param : String
            Password of the connection
        db_name : String
            Database name of the connection

        Returns
        -------
        None.

        """
        self.__host_param = host_param
        self.__use_param = use_param
        self.__password_param = password_param
        self.__dbName = db_name
        self.__conn = None
        self.__cursor = None
        
            
    def connection_db(self):
        """
        Connects to a database with a specified host, user, and password
    
        Parameters
        ----------
        host_param : String
            Hostname of the database
        user_param : String
            Username 
        password_param : String
            Password
    
        Returns
        -------
        MySQLConnection
            Connection to the database
    
        """
        try:
            
            self.__conn = mysql.connector.connect(host=self.__host_param,
                                           user=self.__use_param, 
                                           password=self.__password_param)
            return self.__conn
        except mysql.connector.Error as err:
            print("Connection failed")
            print(err)
    
    def set_cursor(self):
        """
        Sets the cursor

        Returns
        -------
        None.

        """
        self.__cursor = self.__conn.cursor()

    def close(self):
        """
        Closes the cursor and the connection

        Returns
        -------
        None.

        """
        try:
            self.__cursor.close()
            self.__conn.close()
            print("Cursor and connection closed")
        except mysql.connector.Error as err:   
            print("Cannot close cursor and connection")
            print(err)
            
    def create_db(self):
        """
        Creates a databse
        
        Parameters
        ----------
        db_name : String
            name of the database
    
        Returns
        -------
        None.
    
        """
        try:
            self.__cursor.execute("DROP SCHEMA IF EXISTS {}".format(self.__dbName))
            self.__cursor.execute("CREATE DATABASE IF NOT EXISTS {}".format(self.__dbName))
            print("Database {} created".format(self.__dbName))
        except mysql.connector.Error as err:
            print("Cannot create database")
            print(err)
    
    def select_db(self):
        """
        Selects a database to use
    
        Parameters
        ----------
        db_name : String
            The name of the database to be selected
    
        Returns
        -------
        None.
    
        """
        try:
            self.__cursor.execute("USE {}".format(self.__dbName))
            print("{} database selected".format(self.__dbName))
        except mysql.connector.Error as err:
            print("Cannot use database")
            print(err)
    
    def create_table(self, table_name, table_schema):
        """
        Creates a table with a specifed table name and schema
    
        Parameters
        ----------
        table_name : String
            Table name
        table_schema : String
            Table column and type
    
        Returns
        -------
        None.
        
        """
        try:
            self.__cursor.execute("CREATE TABLE {0} ({1})".format(table_name,table_schema))
            print("{} table created".format(table_name))
        except mysql.connector.Error as err:
            print("Cannot create table")
            print(err)
            
    def drop_table(self,table_name):
        """
        Drops a specified table
    
        Parameters
        ----------
        table_name : String
            Name of table to be dropped
    
        Returns
        -------
        None.
    
        """
        try:
            self.__cursor.execute("DROP TABLE IF EXISTS {}".format(table_name))
            print("{} table dropped".format(table_name))
        except mysql.connector.Error as err:
            print("Cannot drop table")
            print(err)

    def populate_table(self,table_name,list_fields, list_values,strformat):
        """
        Populates a specified table
    
        Parameters
        ----------
        conn : MySQLConnection
            Connection used to commit the inserts
        cursor : MySQLCursor
            Cursor of the connection
        table_name : String
            Name of the table to insert into
        list_fields : String
            All fields of the table seperated by a comma
        list_values : list
            A list of tuples which will be inserted into the table
        strformat : String
            String format(%s for every field that will be added, seperated by a comma)

        Returns
        -------
        None.
    
        """
        try:
            insert_statement = "INSERT INTO {0} ({1}) VALUES ({2})".format(table_name,list_fields,strformat)
            self.__cursor.executemany(insert_statement,list_values)
            self.__conn.commit()
            print("Data inserted successfully into {}".format(table_name))
        except mysql.connector.Error as err:
            print("Cannot insert into table")
            print(err)
    
    def select_data_from_table(self,table_name,columns_str):
        """
        Gets specifed data from a specified table with a specied critiera 

        Parameters
        ----------
        table_name : String
            Table name to select from
        columns_str : String
            Columns to select

        Returns
        -------
        records : List
            List of tuples which is all records from the select statement

        """
        try:
            select_statement = "SELECT {0} FROM {1}".format(columns_str,table_name)
            self.__cursor.execute(select_statement)
            records = self.__cursor.fetchall()
            return records
        except mysql.connector.Error as err:
            print("Cannot retrieve data")
            print(err)
    