# -*- coding: utf-8 -*-
"""
@author: Johanna Luangxay (1939023)
@author: Le Duytam Ly (1734119)
"""
from database_management import DBManager
from data_explorer_analysis import DataScience
from database_api import MySQLAPI
from utility import Utility
import getpass

def main():
    """
    Runs the program and interacts with user.

    Returns
    -------
    None.

    """
    tool = Utility()
    
    user = input("Enter your MySql Database user: ")
    #Hidden password works only with external console
    pw = getpass.getpass("Enter your MySql Database password: ")
    connection = MySQLAPI("127.0.0.1",user,pw,"covid_corona_db_luan_ly")
    
    db_man = DBManager(connection)
    choice = input(
        """Do you wish to: 
            [1] Create new/reset database
            [2] Use existing one
        -> """)
    
    choice = tool.tryAgain(["1","2"],choice) 
    if choice == "1":
        db_man.create_new_db()
    else:
        db_man.use_existing_db()      
            
    repeat = input(
        """Do you wish to scrape data?
            [Yes]
            [No]
        -> """).lower()

    repeat = tool.tryAgain(["yes","no"],repeat)
    
    while repeat == "yes":
        choice = input(
            """Do you wish to scrape from today or a previous date:
                [1] Today
                [2] Previous Date
            -> """)
        
        choice = tool.tryAgain(["1","2"],choice)
        
        if choice == "1":
            db_man.scrape_today()
            db_man.populate_corona_table()
        else:
            date = str(input("Please input the desired day: "))
            date = tool.formatDate(date)
            db_man.scrape_old_date(date)
            db_man.populate_corona_table()
        
        repeat = input(
            """Do you wish to scrape another date?:
                [Yes] 
                [No]
            -> """).lower()
        
        repeat = tool.tryAgain(["yes","no"],repeat)
    
    choice = input(
        """Do you wish to analyse data?
            [Yes]
            [No]
        -> """).lower()
        
    choice = tool.tryAgain(["yes","no"],choice)
    
    if choice == "yes":
        list_all_countries = db_man.get_all_country_names()
        
        country = input("Enter the country you would like to analyse: "
            ).lower()
        
        country = tool.tryAgain(list_all_countries,country)
        
        analyser = DataScience(country, connection)
        analyser.compareItself()
        analyser.compareOneNeighbour()
        analyser.compareManyNeighbours()
        analyser.close()
    
    print("Thank you for using our program!")
    db_man.close()
    
    
if __name__ == "__main__":
    main()