# -*- coding: utf-8 -*-
"""
@author: Johanna Luangxay (1939023)
@author: Le Duytam Ly (1734119)
"""

from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
from file_io import FileIO

class ScrapperAPI:
    
    """
    Scrape from the web
    """
    
    def __init__(self, filename, url, is_refresh):
        """
        Initializes the scrapper_api object with a filename, url, and is_refresh
        Gets the bs4 object from the webpage, fetches directly from the website 
        if is_refresh is True. Otherwise, uses a local file

        Parameters
        ----------
        filename : String
            Name of the file to save the html bytes too
        url : String
            URL of the page to get the html bytes from
        is_refresh : Boolean
            True if user wants to fetch the data from the website directly
            False if the user wants to use the local file

        Returns
        -------
        None.

        """
        if is_refresh:
            self.__archive_html_data(filename,url)
            
        self.__bs4_obj = self.__get_bs4_obj(filename)
    
    def __get_html(self, url):
        """
        Gets the html bytes from a specified url
    
        Parameters
        ----------
        url : String
            URL to get the bytes from
    
        Returns
        -------
        html_code : bytes
            Bytes representing the content of the web page
    
        """
        try:
            req= Request(url, headers={'user-agent':'Mozilla/5.0'})
            html_code= urlopen(req)
            return html_code
        except:
            print("An error has occured when making the resquest")
            
    def __archive_html_data(self, filename, url):
        """
        Archives (saves to local) a html page of a specified url
    
        Parameters
        ----------
        filename : String
            Filename to save too
        url : String
            url to save
    
        Returns
        -------
        None.
    
        """
        html_code = self.__get_html(url)
        FileIO.save_to_local_file(filename, html_code.read())
        
    def __get_bs4_obj(self, filename):
        """
        Gets the bs4 object from a specifed file
    
        Parameters
        ----------
        filename : String
            Path to the file which contains html bytes
    
        Returns
        -------
        bs4_obj : BeautifulSoup
            BeautifulSoup object
    
        """
        html_str = FileIO.read_from_file(filename)
        bs4_obj = BeautifulSoup(html_str,features='html.parser')
        return bs4_obj
    
    def get_table_headers(self):
        """
        Gets all table headers (column names) into a list

        Returns
        -------
        cleanHeaders : list
            list containing all column names

        """
        #Finding the first table
        table =  self.__bs4_obj.find('table')
        #Finds all the table headers (column names)
        headers = table.find_all('th')
        #Puts all headers in a list
        cleanHeaders = ["date_retrieved"]
        for header in headers:
            cleanHeaders.append(header.text.strip())
        
        #Removes "\n", "\xa0", " " characters 
        cleanHeaders = [e.replace("\n", "") for e in cleanHeaders]
        cleanHeaders = [e.replace("\xa0", " ") for e in cleanHeaders]
        cleanHeaders = [e.replace(" ", "") for e in cleanHeaders]
        #Replaces "#" with "rank_num" (can't insert "#" into mysql)
        cleanHeaders = [e.replace("#", "rank_num") for e in cleanHeaders]
        #Replaces "," and "/" with an underscore (can't insert these characters in mysql)
        cleanHeaders = [e.replace(",", "_") for e in cleanHeaders]
        cleanHeaders = [e.replace("/", "_") for e in cleanHeaders]
        #Adds the backticks for the headers to be inserted
        cleanHeaders = ['`' + e + '`' for e in cleanHeaders]
        return cleanHeaders
    
    def get_table_records(self,table_id,date):
        """
        Gets all the records for a specific table

        Parameters
        ----------
        table_id : String
            Table id to get the records from

        Returns
        -------
        TYPE list
            List of tuples that contains all records from a given table.

        """
        #Finding the table with its id
        table =  self.__bs4_obj.find('table', id=table_id)
        #All possible styles for a table row
        possible_styles = {"style":["","background-color:#EAF7D5","background-color:#F0F0F0"]}
        #Finds all table rows with all possible styles
        records = table.find_all("tr",possible_styles)
        #Loops through all tr
        clean_records = []
        for record in records:
            #Loops through all td and appends all td to a list
            record_entry = [str(date)]
            for td in record.find_all("td"):
                record_entry.append(td.text)
            #Appends record_entry list to clean record_list casting it into tuple
            clean_records.append(tuple(self.clean_record(record_entry)))
        return clean_records
    
    def clean_record(self,record):
        """
        Functions that cleans data.
        
        Parameters
        ----------
        record : List
            Contains an uncleaned list of record for corona table.

        Returns
        -------
        clean_record : List
            Contains an cleaned list of record for corona table.

        """
        
        clean_record = []
        for e in record:
            if e:
                try:
                    clean_record.append(eval(e.replace(",","")))
                except:
                    if e == "N/A":
                        clean_record.append(None)
                    elif e == " ":
                        clean_record.append(None)
                    else:
                        clean_record.append(e.replace(",",""))
            else:
                clean_record.append(None)
        return clean_record
    