# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 17:22:32 2021

@author: Johanna Luangxay (1939023)
@author: Le Duytam Ly (1734119)
"""
from datetime import date

class Utility:
    """
    Class with some utility functions
    """
    def formatDate(self,day):
        """
        Formats the given date of the user to follow convention of the filename.
    
        Parameters
        ----------
        day : String
            Entered day.
    
        Returns
        -------
        day : String
            Final date with current month and year.
    
        """
        #If the date is 1 digit, appends the 0 before
        if len(day) == 1:
            day = "0"+day
        day = date.today().strftime("%Y-%m-")+day
        return day
    
    def __validateInput(self,valideInput,usrInput):
        """
        Check if input is among the list of valide input

        Parameters
        ----------
        valideInput : List
            Possible valide input.
        usrInput : String
            Input of user.

        Returns
        -------
        bool
            Whether input is part of the valide input list.

        """
        if usrInput not in valideInput:
            return False
        else:
            return True
    
    def tryAgain(self,valideInput,usrInput):
        """
        Forces user to enter valide input

        Parameters
        ----------
        valideInput : List
            Possible valide input.
        usrInput : String
            Input of user.

        Returns
        -------
        usrInput : String
            Final valide input.

        """
        valideChoice = self.__validateInput(valideInput,usrInput)
        while not valideChoice:
            usrInput = input("You did not entered a valid option. Please try again! ")
            if isinstance(usrInput, str):
                usrInput = usrInput.lower()
            valideChoice = self.__validateInput(valideInput,usrInput)
        return usrInput