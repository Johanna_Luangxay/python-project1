# -*- coding: utf-8 -*-
"""
@author: Le Duytam Ly (1734119)
"""

class FileIO:
    """
    Reads and Writes file
    """
    
    def save_to_local_file(filename, html_bytes):
        """
        Saves the bytes to a local file for reuse
    
        Parameters
        ----------
        filename : String
            Filename to write to
        html_bytes : byte
            Bytes to write to the file
    
        Returns
        -------
        None.
    
        """
        try:
            file = open(filename,"wb")
            file.write(html_bytes)
        except:
            print("An error has occured when writing to the file")
        finally:
            file.close()
            
    def read_from_file(filename):
        """
        Function that reads from a file of byte
    
        Parameters
        ----------
        filename : String
            File of bytes to read from
    
        Returns
        -------
        html_str : bytes
            String which contains the bytes. Used by BeautifulSoup
    
        """
        try:
            file = open(filename,"rb")
            html_str = file.read()
            return html_str
        except:
            print("AN error has occured when reading the file")
        finally:
            file.close()